from django.shortcuts import redirect
from django.views import generic


class HomeView(generic.TemplateView):
    template_name = "pages/home.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.COOKIES.keys() >= {"root_instance", "user_id", "auth_token"}:
            return redirect("settings")
        return super().dispatch(request, *args, **kwargs)


class SettingsView(generic.TemplateView):
    template_name = "pages/settings.html"
